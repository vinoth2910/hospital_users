<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HospitalUsers extends Model
{
    
	protected $table = "hospital_users";
    public $timestamps = false;

    
    protected $fillable = [
        'userType'
    ];    
	
}


