<?php

use Illuminate\Database\Seeder;

class HospitalUsers extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		 //Remove the existing user from table
         //DB::table('hospital_users')->truncate();

		 //Static record for testing purpose
		 for($i=0; $i<12; $i++){
			 DB::table('hospital_users')->insert([
				'firstName' => Str::random(10),
				'lastName' => Str::random(10),
				'email' => Str::random(10).'@gmail.com',
				'phone' => rand(9100000000, 9999999999),
				'userType' => 1,
				'status' => 1,
				'lastLoggedIn' => "2021-07-06 03:21:07",
				'authorized' => "2021-07-06 03:21:07",
			]);
		 }
		
    }
}
