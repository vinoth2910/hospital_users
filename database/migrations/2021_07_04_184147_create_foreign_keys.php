<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*Schema::table('hospital_users', function(Blueprint $table) {
			$table->foreign('userType')->references('id')->on('user_type')
						->onDelete('restrict')
						->onUpdate('restrict');
		});*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hospital_users', function(Blueprint $table) {
			$table->dropForeign('hospital_users_userType_foreign');
		});
    }
}
