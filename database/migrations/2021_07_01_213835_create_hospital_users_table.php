<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHospitalUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hospital_users', function (Blueprint $table) {
            $table->increments('id');
			$table->string('firstName',100);
			$table->string('lastName',100);
			$table->string('email')->unique();
			$table->biginteger('phone');
			$table->integer('userType')->unsigned()->default('0');
			$table->dateTime('lastLoggedIn');
			$table->dateTime('authorized');
			$table->biginteger('status');
			$table->timestamp('created_dt')->useCurrent();
            $table->timestamp('modified_dt')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hospital_users');
    }
}
