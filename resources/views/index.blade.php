@extends('main')
   
@section('main-content')
   
   <div class="container">
  <div class="jumbotron">
    <h1>Laravel Technical Test</h1>
  </div>    
 <div class="panel panel-primary">
      <div class="panel-heading">
	
      </div>
      <div class="panel-body">
	 	<table class="table table-hover table-bordered table-stripped display" id="data-tables">
	 		<thead>
	 			<tr>
	 			<th>Full Name</th>
	 			<th>Email</th>
	 			<th>Last Logged In</th>
	 			<th>Authorized</th>
	 			<th>User Type</th>
	 			</tr>
	 		</thead>
	 		<tbody>
	 		    @foreach ($users as $user)
	 			<tr>
					<td>{{ $user->firstName }} {{ $user->lastName }}</td>
					<td>{{ $user->email }}</td>
					<td>{{ date("d/m/Y h:i:s", strtotime($user->lastLoggedIn)) }}</td>
					<td><p style="background-color:#750046; color:#ffffff; padding:2px; width:85%; text-align:center">{{ date("d/m/Y h:i:s", strtotime($user->authorized)) }}</p></td>
					<td>
					<p style="cursor:pointer; background-color:#750046; color:#ffffff; padding:2px; width:85%; text-align:center" title="Edit" onclick="showModal('{{ $user->id }}','{{ $user->userType }}')" id="doctorType{{ $user->id }}">{{ $user->doctorTypes }}</p></td>	 			
	 			</tr>
	 			@endforeach
	 		</tbody>
	 	</table>
	 	<p class="pull-right">
	 	</p>
   	  </div>
    </div>
</div>
<div class="modal fade" id="showModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" >Confirmation</h4><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">
				<div class="clearfix">
					<div class="row form-group">						
						<div class="col-md-12">
						<select class="form-control" id="newUserType">
							@foreach ($user_types as $user_type)
								<option value="{{ $user_type->id }}">{{ $user_type->doctorTypes }}</option>
							@endforeach
						</select>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
					<input type="hidden" value="" id="saveUsertypeID" />
					<input type="hidden" value="" id="userID" />
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" onclick="updateUserType()">Update</button>
                </div>
		</div>
	</div>
</div>
<div class="spinner" role="status">
  <span class="sr-only">Loading...</span>
</div>
    
@endsection