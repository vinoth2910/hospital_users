<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/app.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/jquery.dataTables.min.css') }}">
    <title>Laravel Technical Test</title>
  </head>
  <body>
    
   
    @section('main-content')

    // Here comes the extended codes

    @show

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script type="text/javascript" src="{{ asset('assets/js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/app.js') }}"></script>
	<script>

		function updateUserType(){
		
			var usertypeID = $("#newUserType").val();
			var usertypeText = $('#newUserType').find(":selected").text();
			var userId = $("#userID").val();
			$(".spinner").show();
			$.ajax({
				method: 'POST',
				url:'/saveUserType',
				dataType: "json",
				data: {"_token": "{{ csrf_token() }}", id: userId, userType: usertypeID},
				beforeSend: function (xhr) {
					xhr.overrideMimeType("text/plain; charset=x-user-defined");
				}
			}).done(function (data) {
				if (data.success) {			
					$("#doctorType"+userId).text(usertypeText);				
					$(".spinner").hide();
					$('#showModal').modal('toggle');	
				}
			}).fail(function (jqXHR, textStatus) {
			}).always(function () {
			});
		}	
	</script>
  </body>
</html>